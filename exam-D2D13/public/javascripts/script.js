document.getElementById('addGare').addEventListener('submit', function(evt){
    evt.preventDefault();
    var form = evt.target;
    var datas = {};
    form.querySelectorAll('[name]').forEach(function(el){
        datas[el.getAttribute('name')] = el.value
    });
    makeRequest('', form.getAttribute('method'), datas, function(res){
        var span = document.createElement("span");
        span.innerHTML = res.response;
        var forDelete = span.querySelector(".delete");
        var forUpdate = span.querySelector(".update");
        toDelete(forDelete);
        toUpdate(forUpdate);
        var container = document.querySelector('.gares');
        container.appendChild(span.firstChild);
    })      
})

var toDelete = function(element){
    element.addEventListener('click', function(){
            makeRequest(this.getAttribute('data-id'),'DELETE', null, function(re){
            element.closest('.gares').remove()
        });
    })
}


document.querySelectorAll(".delete").forEach(toDelete);



var toUpdate = function(element){
    element.addEventListener('click', function(){
        var name = this.getAttribute('data-name') == "false";
        makeRequest(this.getAttribute('data-id'), 'PUT', {name:name}, function(res){
            var span = document.createElement('span');
            span.innerHTML = res.response;
            var forDelete = span.querySelector(".delete");
            var forUpdate = span.querySelector(".update");
            toUpdate(forUpdate);
            toDelete(forDelete);
            element.closest('.gares').replaceWith(span.firstChild);
        });
    })
}
document.querySelectorAll(".update").forEach(toUpdate);


function makeRequest(id, method, name, callback) {
    httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function(){
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {
                callback(JSON.parse(httpRequest.responseText))
            } else {
                alert('Try again');
            }
        }
    };
httpRequest.open(method, '/' + id);
httpRequest.setRequestHeader('Content-type', 'application/json');
httpRequest.send(name ? JSON.stringify(name) : null);
}


