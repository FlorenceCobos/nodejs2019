var express = require('express');
var router = express.Router();
var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var ObjectId = mongodb.ObjectID ;

MongoClient.connect('mongodb://localhost:27017/examD2D13', { useNewUrlParser: true }, function (err, client) {
  if (err) throw err

  let db = client.db('examD2D13')
  
  router.get('/', function(req, res, next) {
    db.collection('gares').find({}).toArray(function (err, result){
      res.render('home', { title: 'Toutes nos gares', gares: result});
      })
  });

  router.delete('/:id', function(req, res, next){
    db.collection('gares').deleteOne({'_id': ObjectId(req.params.id)}, function(err, result){
      if(err) return next(err);
      return res.json(result)
    })
  })

  router.post('/', function(req, res, next) {
    db.collection('gares').insertOne(
      { nom: req.body.nom,
        datecreation: new Date(),
        siret: 'None',
        theme: 'DEPLACEMENT',
        idexterne: req.body.idexterne,
        soustheme: 'Gare SNCF',
        gid: req.body.gid, 
        identifiant: req.body.identifiant, 
      }, 
  function(error, result) {
          if(err) return next(err);
            db.collection('gares').findOne({_id: result.insertedId}, function(err, resultats) {
              if(err) return next(err);
              res.render('index', {gare : resultats}, function(err, html) {
                if(err) return next(err);
                return res.json(
                  {response : html})
        });
      })
    })
  })
})


module.exports = router;
